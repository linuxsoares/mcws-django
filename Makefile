clean:
	find . -name "*.pyc" | xargs rm -rf
	find . -name "*.pyo" | xargs rm -rf
	find . -name "__pycache__" -type d | xargs rm -rf
	rm -f .coverage
	rm -rf htmlcov/
	rm -f coverage.xml
	rm -f *.log

install:
	pip install -r requirements.txt

run:
	./manage.py runserver

flake8:
	flake8 --show-source .
