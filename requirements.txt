django-all-access
pycrypto==2.4
requests==2.0
requests_oauthlib==0.4.2
oauthlib==0.6.2
django==1.10
flake8
