from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth.views import logout_then_login
from facebook.views import home

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('allaccess.urls')),
    url(r'^logout/$', logout_then_login, name='logout'),
    url(r'^$', home, name='home'),
]
